package com.example.spring.service;

import com.example.spring.entity.Entrepreneur;

import java.util.List;

public interface EmployesServices {
    public Entrepreneur saveEntrepreneur(Entrepreneur entrepreneur);
    public List<Entrepreneur> Alltrepreneur();
    public Entrepreneur updateEmployes(Entrepreneur entrepreneur,long id);
    public Entrepreneur getById(long id);
    public void  deleteEntrepreneur(long id);
}
