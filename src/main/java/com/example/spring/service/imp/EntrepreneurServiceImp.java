package com.example.spring.service.imp;
//https://dev-api-odrive.groupeidyal.com/admin/ admin diango
import com.example.spring.Exceptions.Exeception;
import com.example.spring.entity.Entrepreneur;
import com.example.spring.repository.EntrepeneurRepository;
import com.example.spring.service.EmployesServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EntrepreneurServiceImp implements EmployesServices {

    private EntrepeneurRepository entrepeneurRepository;

    public EntrepreneurServiceImp(EntrepeneurRepository entrepeneurRepository) {
        this.entrepeneurRepository = entrepeneurRepository;
    }

    @Override
    public Entrepreneur saveEntrepreneur(Entrepreneur entrepreneur) {
        return entrepeneurRepository.save(entrepreneur);
    }

    @Override
    public List<Entrepreneur> Alltrepreneur() {
        return entrepeneurRepository.findAll();
    }

    @Override
    public Entrepreneur updateEmployes(Entrepreneur entrepreneur, long id) {
      Entrepreneur entrepreneur1=entrepeneurRepository.findById(id).orElseThrow(()->new Exeception("L'entrepreneur","id",id));
      entrepreneur1.setLastname(entrepreneur.getLastname());
      entrepreneur1.setFirstname(entrepreneur.getFirstname());
      entrepreneur1.setEmail(entrepreneur.getEmail());
      entrepeneurRepository.save(entrepreneur1);

        return entrepreneur1;
    }

    @Override
    public Entrepreneur getById(long id) {
            return  entrepeneurRepository.findById(id).orElseThrow(() ->
            new Exeception("L'entrepreneur ","id",id));

    }

    @Override
    public void deleteEntrepreneur(long id) {
      Entrepreneur entrepreneur=entrepeneurRepository.findById(id).orElseThrow(()->new Exeception("Entrepreneur","id",id));
      entrepeneurRepository.delete(entrepreneur);
      System.out.println("deleted with sucess!!!!!");
    }
}
