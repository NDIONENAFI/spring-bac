package com.example.spring;

import com.example.spring.entity.Entrepreneur;
import com.example.spring.repository.EntrepeneurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {


	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

//	@Bean
//	public CommandLineRunner demo(EntrepeneurRepository repository) {
//		return (args) -> {
//			Entrepreneur entrepreneur = new Entrepreneur("Ass", "Malick", "malickass@gmail.com");
//			repository.save(entrepreneur);
//			System.out.println("la liste des entrepreneur actuellement en base donne" + repository.findAll());
//
//		};
//	}



}
