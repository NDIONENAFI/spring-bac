package com.example.spring.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Table(name = Entrepreneur.TABLE_NAME)

public class Entrepreneur implements Serializable {
    public static final String TABLE_NAME ="entrepreneur";
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    @Column(name ="first_name",nullable = false)
    private String firstname;
    @Column(name ="last_name")
    private String lastname;
    @Column(name ="email_id",nullable = false)
    private String email;
    @OneToMany(mappedBy = "entrepreneur",fetch = FetchType.LAZY)
    private Collection<WorkSpace>workSpaces;

    public Entrepreneur() {
    }

    public Entrepreneur( String firstname, String lastname, String email) {

        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Collection<WorkSpace> getWorkSpaces() {
        return workSpaces;
    }

    public void setWorkSpaces(Collection<WorkSpace> workSpaces) {
        this.workSpaces = workSpaces;
    }
}
