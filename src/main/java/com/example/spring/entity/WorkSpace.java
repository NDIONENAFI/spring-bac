package com.example.spring.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name =WorkSpace.TABLE_NAME)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class WorkSpace implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    public static final String TABLE_NAME ="work_space";
    private String nomspace;

    private String datecreation ;
    @ManyToOne
    private Entrepreneur entrepreneur;



    public String getNomspace() {
        return nomspace;
    }

    public void setNomspace(String nomspace) {
        this.nomspace = nomspace;
    }

    public String getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(String datecreation) {
        this.datecreation = datecreation;
    }

    public Entrepreneur getEntrepreneur() {
        return entrepreneur;
    }

    public void setEntrepreneur(Entrepreneur entrepreneur) {
        this.entrepreneur = entrepreneur;
    }
}
