package com.example.spring.repository;

import com.example.spring.entity.Entrepreneur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntrepeneurRepository extends JpaRepository<Entrepreneur,Long> {

}
