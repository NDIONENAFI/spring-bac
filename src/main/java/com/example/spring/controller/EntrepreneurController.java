package com.example.spring.controller;

import com.example.spring.entity.Entrepreneur;
import com.example.spring.repository.EntrepeneurRepository;
import com.example.spring.service.EmployesServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
//RestController= @ResponseBody + Controller
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/emp/")
public class EntrepreneurController {

    private EmployesServices employesServices;

    public EntrepreneurController(EmployesServices employesServices) {
        this.employesServices = employesServices;
    }

    //recuperre la liste des entrepreneur
    @GetMapping("/entrepreneur")
    public List<Entrepreneur>geAll(){
        return employesServices.Alltrepreneur();
    }
    // add employes
    @PostMapping ("/entrepreneur")
    public ResponseEntity<Entrepreneur>  createEntrepeneur(@RequestBody Entrepreneur entrepreneur){
        return new ResponseEntity(employesServices.saveEntrepreneur(entrepreneur), HttpStatus.CREATED)   ;
    }

    @GetMapping("/entrepreneur/{id}")
    public ResponseEntity<Entrepreneur>getById(@PathVariable("id")  Long id){
      return new ResponseEntity(employesServices.getById(id),HttpStatus.OK);
    }
     //update emtrepreneur
    @PutMapping("/entrepreneur/{id}")
    public ResponseEntity<Entrepreneur>UpdateEmployes(@PathVariable("id")  Long id,@RequestBody Entrepreneur entrepreneurupdate){
        return new ResponseEntity(employesServices.updateEmployes(entrepreneurupdate,id),HttpStatus.OK);
    }
   // Delete a employes
   @DeleteMapping("/entrepreneur/{id}")
   public ResponseEntity<String> deleteEntrepreneur(@PathVariable("id")  Long id ){
        employesServices.deleteEntrepreneur(id);
       return new ResponseEntity<String>("Entrepeneur supprime avec success",HttpStatus.OK);
   }



}
