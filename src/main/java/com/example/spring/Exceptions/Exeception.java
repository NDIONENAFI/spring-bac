package com.example.spring.Exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class Exeception extends RuntimeException{
    private static final  long serialVersionUID=1L;
    private String Nomressouce;
    private String typeressource;
    private Object ressource;

   public  Exeception(String Nomressouce,String typeressource,Object ressource){
       super(String.format("%s est introuvrable avec '%s' de : '%s'",Nomressouce,typeressource,ressource));
       this.Nomressouce=Nomressouce;
       this.typeressource=typeressource;
       this.ressource=ressource;
   }

    public String getNomressouce() {
        return Nomressouce;
    }
    public String getTyperessource() {
        return typeressource;
    }
    public Object getRessource() {
        return ressource;
    }


}
